 package commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"aagregator/server"
	"aagregator/logger"
)

var log_level string
var port int
var entity_id_file string
var simulate bool
var aagregator_url string
var aagregator_path string
var hosts_file string

var ServerCmd = &cobra.Command{
	Use:   "aagregator",
	Short: "Shibboleth attribute aggregator cli",
	Long: `Control interface for the shibboleth attribute aggregator.`,
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of docker-clarin",
	Long:  `All software has versions. This is docker-clarin's.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		fmt.Printf("Shibboleth test server v%s by CLARIN ERIC\n", "1.0.0-beta")
		return nil
	},
}

var startCmd = &cobra.Command{
	Use:   "start",
	Short: "start server",
	Long: `Start server`,
	Run: func(cmd *cobra.Command, args []string) {
		logPtr := logger.NewLogger(log_level)
		logPtr.Info("Log level argument: %s, using log level: %s", log_level, logPtr.GetLevelAsString())
		server.StartServerAndblock(logPtr, port, entity_id_file, simulate, aagregator_url, aagregator_path, hosts_file)
	},
}

func Execute() {
	startCmd.Flags().IntVarP(&port, "port", "p", 8080, "Base port to run the server on")
	startCmd.Flags().StringVarP(&entity_id_file, "entity_ids", "i", "/entity_ids.properties", "Path to properties file with application id to entity id mapping configuration.")
	startCmd.Flags().BoolVarP(&simulate, "simulate", "s", false, "Simulate statistics submission. This mode will not send any data to the remote aagregator endpoint.")
	startCmd.Flags().StringVarP(&aagregator_url, "aagregator_url", "U", "https://clarin-aa.ms.mff.cuni.cz", "Base URL for the aagregator endpoint.")
	startCmd.Flags().StringVarP(&aagregator_path, "aagregator_path", "P", "/aaggreg/v1/got", "Path under the base URL for the aagregator endpoint.")
	startCmd.Flags().StringVarP(&hosts_file, "hosts", "H", "/hosts.properties", "Path to properties file with host mapping configuration.")

	ServerCmd.PersistentFlags().StringVarP(&log_level, "log_level", "v", logger.LevelToString(logger.GetDefaultLogLevel()), "Log level, supported values: TRACE, DEBUG, INFO, WARN and ERROR.")
	ServerCmd.AddCommand(versionCmd)
	ServerCmd.AddCommand(startCmd)
	ServerCmd.Execute()
}