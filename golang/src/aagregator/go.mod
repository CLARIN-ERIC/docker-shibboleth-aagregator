module aagregator

go 1.21.3

require (
	github.com/spf13/cobra v1.8.0
	gopkg.in/alexcesaro/statsd.v2 v2.0.0
	launchpad.net/xmlpath v0.0.0-20130614043138-000000000004
)

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
