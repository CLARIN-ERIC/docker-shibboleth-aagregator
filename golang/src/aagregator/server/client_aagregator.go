package server

import (
	"net/url"
	"fmt"
	"time"
	"io/ioutil"
	"encoding/json"
	"net/http"
)

type ApiResponse struct {
	Ok bool `json:"ok"`
}

func (h *Handler) sendInfo(aInfo *attributeInfo) error {
	aagUrl, err := url.Parse(h.aggregator_url)
	if err != nil {
		return err
	}

	aagUrl.Path += h.aggregator_path
	parameters, err := url.ParseQuery(aagUrl.RawQuery)
	if err != nil {
		return err
	}
	parameters.Add("idp", aInfo.idp)
	parameters.Add("sp", aInfo.sp)
	parameters.Add("timestamp", aInfo.ts)
	parameters.Add("warn", aInfo.suspicious)
	for _, attr := range aInfo.attributes {
		if attr != "" {
			h.logPtr.Debug("Adding attributes[]=%s", attr)
			parameters.Add("attributes[]", attr)
		}
	}
	aagUrl.RawQuery = parameters.Encode()

	if(!h.simulate) {
		h.logPtr.Debug("Submitting statistics: %s\n", aagUrl.String())
		client := &http.Client{
			Timeout: time.Second * 10,
		}

		response, err := client.Get(aagUrl.String())
		if err != nil {
			return err
		}

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return err
		}

		var s = ApiResponse{}
		err = json.Unmarshal(body, &s)
		if (err != nil) {
			return err
		}

		if (s.Ok) {
			h.logPtr.Info("Successfully submitted statistics to %s%s",h.aggregator_url, h.aggregator_path)
		} else {
			h.logPtr.Error("Failed to submit statistics to %s", h.aggregator_url)
			h.logPtr.Error("Response: HTTP %s", response.Status)
			h.logPtr.Error("Response body: [%s]\n", string(body))
			return fmt.Errorf("Failed to submit statistics to %s%s. Http response: ", h.aggregator_url, h.aggregator_path, response.Status)
		}
	} else {
		h.logPtr.Debug("Simulating submission of statistics: %s", aagUrl.String())
	}

	return nil
}