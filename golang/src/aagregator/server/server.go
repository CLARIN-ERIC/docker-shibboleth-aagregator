package server

import (
	"net/http"
	"strconv"
	"aagregator/logger"
	"os"
	"bufio"
	"io"
	"strings"
)

func loadProperties(logPtr *logger.Logger, filename string) (map[string]string) {
	result := map[string]string{}
	logPtr.Debug("Loading SP entityID mappings from: %s.", filename)
	loaded_mappings, err := readConfig(filename)
	if err != nil {
		logPtr.Debug("Failed to load properties from %s. Error: %s", filename, err)
	}
	if len(loaded_mappings) > 0 {
		result = loaded_mappings
	}
	return result
}

func StartServerAndblock(logPtr *logger.Logger, port int, sp_entity_id_mapping_file string, simulate bool, aagregator_url, aagregator_path string, host_mapping_file string) {
	entity_id_map := loadProperties(logPtr, sp_entity_id_mapping_file)
	if _, ok := entity_id_map["default"]; !ok {
		logPtr.Error("default entry for application id -> entity id is required")
		return
	}
	hosts := loadProperties(logPtr, host_mapping_file)

	//host_map
	h := Handler{
		logPtr: logPtr,
		entity_id_map: entity_id_map,
		simulate: simulate,
		aggregator_path: aagregator_path,
		aggregator_url: aagregator_url,
		hosts: hosts,
	}

	//print configuration
	if len(entity_id_map) <= 0 {
		logPtr.Info("No entity id mappings configured.")
	} else {
		logPtr.Info("Entity ID maps:")
		for key, value := range entity_id_map {
			logPtr.Info("  %s=%s", key, value)
		}
	}
	if len(hosts) <= 0 {
		logPtr.Info("No host mappings configured.")
	} else {
		logPtr.Info("Host maps:")
		for key, value := range hosts {
			logPtr.Info("  %s=%s", key, value)
		}
	}
	logPtr.Info("Simulation: %t", simulate)
	logPtr.Info("Aagregator endpoint: %s%s", aagregator_url, aagregator_path)
	logPtr.Info("Starting http server on port: %d", port)

	//Register handlers
	http.HandleFunc("/", h.handler)

	//Start server
	err := http.ListenAndServe(":"+strconv.Itoa(port), nil)
	if err != nil {
		logPtr.Error("Failed to start server: %v", err)
	}
}

func readConfig(filename string) (map[string]string, error) {
	props := map[string]string{}
	file, err := os.Open(filename)
	if err != nil {

		return nil, err
	}
	defer file.Close()

	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		if !strings.HasPrefix(line, "#") {
			parts := strings.Split(line, "=")
			if len(parts) == 2 {
				val := parts[1]
				val = strings.TrimSuffix(val, "\n")
				val = strings.TrimPrefix(val, "\"")
				val = strings.TrimSuffix(val, "\"")
				props[parts[0]] = val
			}
		}
	}

	return props, nil
}


