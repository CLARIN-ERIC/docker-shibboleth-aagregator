#!/bin/bash

#_GOPATH="/Users/wilelb/Code/work/clarin/git/infrastructure2/golang"
_BINARY="aagregator"

PROJECT_PATH="golang/src/aagregator"

init_data (){
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    if [ "${LOCAL}" -eq 0 ]; then
        #Remote / gitlab ci
	echo "Building ${_BINARY}_linux remotely"
        cd .. || return
	docker run --rm -v "$PWD/${PROJECT_PATH}":/go/src/aagregator -w /go/src/aagregator golang:1.21 make
        mv "./${PROJECT_PATH}/${_BINARY}_"* ./image && \
        cd image || return
    else
        cd .. || return
        echo "Building ${_BINARY}_linux locally"
	docker run --rm -v "$PWD/${PROJECT_PATH}":/go/src/aagregator -w /go/src/aagregator golang:1.21 make
        mv "./${PROJECT_PATH}/${_BINARY}_"* ./image
        cd ./image || return
    fi
}

cleanup_data () {
    echo "Removing ${_BINARY}_*"
    rm -f "${_BINARY}_"*
}
